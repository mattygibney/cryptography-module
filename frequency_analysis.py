#Simple frequency analysis code

def sort(letters,freq):
    for i in range(0,len(freq)):
        for j in range(0,len(freq)):
            if(freq[i]>freq[j]):
                freq[i],freq[j] = freq[j],freq[i]
                letters[i],letters[j] = letters[j], letters[i]

    return letters, freq
def countFrequency(message):
    letters = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','w','x','y','z']
    freq = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    for char in message.lower():
        if char in letters:
            freq[letters.index(char)] += 1
    return letters,freq 

letters,freq = countFrequency(input("Please enter your string:\n"))

letters, freq = sort(letters,freq)
for i in range(len(letters)):
    print(letters[i] + ":" + str(freq[i]))
            
