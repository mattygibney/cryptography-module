#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LENGTH 250

//Function to shift characters by key
char* shift(char *plain_text, int key){
	for(int i=0;i<strlen(plain_text);i++){
		plain_text[i] = plain_text[i] + (key % 26);
	}
	return plain_text;
}

int main(){
	char *text = (char *)malloc(sizeof(char)*MAX_LENGTH);
	fgets(text,MAX_LENGTH,stdin);
	shift(text,3);
	printf("%s\n", text);
	shift(text,-3);
	printf("%s\n", text);
}

