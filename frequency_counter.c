#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Basic structure to store characters and count
struct freq {
	int size;
	char *characters;
	int *frequencies;
};

struct freq* count(char *string){
	struct freq *string_freq=(struct freq*)malloc(sizeof(struct freq));
	//Allocate enough size for unique strings
	string_freq->characters = (char *)malloc(sizeof(char)*(strlen(string)));
	string_freq->frequencies = (int *)malloc(sizeof(int)*(strlen(string)));
	string_freq->size = 0;


	int found=0;
	for(int i=0; i<strlen(string); i++){
		//Search to see if we alread have the character
		found=0;
		for(int j=0; j < string_freq->size; j++){
			if(string_freq->characters[j] == string[i]){
				found=1;
				string_freq->frequencies[j] += 1;
			}
		}
		//Add the character if not found
		if(found == 0){
			//Update count
			string_freq->characters[string_freq->size] = string[i];
			string_freq->frequencies[string_freq->size] = 1;
			string_freq->size += 1;
		}
	}
	//Sort the arrays
	return string_freq;
}
void print_struct(struct freq* string_freq){
	for(int i=0; i<string_freq->size;i++){
		if(string_freq->characters[i] == '\n'){
			printf("\\n: %i\n", string_freq->frequencies[i]);
		}
		else{ 
			printf("%c: %i\n",string_freq->characters[i], string_freq->frequencies[i]);
		}
	}
}

void free_struct(struct freq* string_freq){
	free(string_freq->characters);
	free(string_freq->frequencies);
	free(string_freq);
}

//Dont judge the bubble sort
struct freq* sort_struct(struct freq* string_freq){
	for(int i=0; i<string_freq->size-1;i++){
		for(int j=0;j<string_freq->size-1;j++){
			if(string_freq->frequencies[j] < string_freq->frequencies[j+1]){
				int temp_num;
				char temp_char;
				temp_num = string_freq->frequencies[j];
				string_freq->frequencies[j] = string_freq->frequencies[j+1];
				string_freq->frequencies[j+1] = temp_num;
				temp_char = string_freq->characters[j];
				string_freq->characters[j] = string_freq->characters[j+1];
				string_freq->characters[j+1] = temp_char;
			}
		}
	}
	return string_freq;

}


int main(int argc, char **argv)
{
	FILE *infile;
	char *buffer;
	long numbytes;

	struct freq *string_freq;
	//Read file into buffer
	if(argc == 2){
		infile = fopen(argv[1], "r");
	}
	else{
		printf("Usage: ./counter <textfile.txt>\n");
		return 1;
	}
	if(infile==NULL){
		return 1;
	}
	fseek(infile, 0L, SEEK_END);
	numbytes = ftell(infile);
	fseek(infile, 0L, SEEK_SET);
	buffer = (char *)calloc(numbytes+1, sizeof(char));
	fread(buffer, sizeof(char), numbytes,infile);
	fclose(infile);

	string_freq = count(buffer);
	print_struct(sort_struct(string_freq));
	free_struct(string_freq);
	free(buffer);

}
